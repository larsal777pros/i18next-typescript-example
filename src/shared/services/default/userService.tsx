import BaseService from '../baseService';
import {IUser} from 'shared/models/default/userModel';
const requestUrl = 'http://localhost:8080/api/user';

export default class UserService extends BaseService<IUser> {
constructor (){
        super(requestUrl);
    }
};
