import axios from 'axios';

export default class appConfig {
    public baseApiUrl: string;

    constructor() {
        this.baseApiUrl = 'http://localhost:8080/api';
    }

    public initAxiosInterceptors() {
        axios.defaults.withCredentials = true;
        axios.defaults.headers.common = {
            'Content-Encoding': 'gzip'
        };
        if (axios.interceptors) {
            axios.interceptors.request.use(this.onRequestSuccess);
            axios.interceptors.response.use(this.onResponseSuccess, this.onResponseError);
        }
    }

    public onRequestSuccess(config:any) {
        return config;
    }

    public onResponseSuccess(response:any) {
        return response;
    }

    public onResponseError(err:any) {
        const status = err && err.status ? err.status : err && err.response ? err.response.status : undefined;
        if (!status) {
            return;
        }
        console.log(status);
        if (status === 400) {
            //handle 400 status -> bad request
        } else if (status === 401) {
            //handle 401 status -> Unauthorized
        } else if (status === 403) {
            //handle 403 status -> Forbidden
        } else if (status === 404) {
            //handle 404 status -> Not Found
        } else if (status === 405) {
            //handle 405 status -> Method Not Allowed
        } else if (status === 408) {
            //handle 408 status -> Request Timeout
        } else if (status === 409) {
            //handle 409 status -> Conflict
        } else if (status === 500) {
            //handle 500 status -> Internal Server error
        } else if (status === 501) {
            //handle 501 status -> Not Implemented
        } else if (status === 502) {
            //handle 502 status -> Bad Gateway
        } else if (status === 504) {
            //handle 504 status -> Gateway Timeout
        }
        return err;
    }
}
